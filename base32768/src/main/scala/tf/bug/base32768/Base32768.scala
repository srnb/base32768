package tf.bug.base32768

import java.util.concurrent.TimeUnit

import cats._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import fs2._
import scodec.bits._
import tf.bug.base32768.buildinfo.Blocks

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

object Base32768 {

  def encode[F[_]]: Pipe[F, Byte, Char] = in => {
    val bit8vectors = in.map(BitVector.fromByte(_))
    val bits = bit8vectors.flatMap(v => Stream.emits(v.toIndexedSeq))
    val bit15vectors = bits.chunkN(15).map(c => BitVector.bits(c.toArray))
    bit15vectors.through(encode1)
  }

  def encode1[F[_]]: Pipe[F, BitVector, Char] = _.map { bits =>
    val adjBits =
      if (bits.sizeLessThan(7)) bits.not.padRight(7).not
      else if (bits.sizeLessThan(15)) bits.not.padRight(15).not
      else bits
    val byte = adjBits.padLeft(16).toShort()
    val oInt =
      if (bits.sizeLessThan(8)) Blocks.lookupEncodeOne(byte)
      else Blocks.lookupEncodeZero(byte)
    oInt.toChar
  }

  def decode[F[_]: RaiseThrowable]: Pipe[F, Char, Byte] = in => {
    val bit15vectors = in.through(decode1)
    val bits = bit15vectors.flatMap(v => Stream.emits(v.toIndexedSeq))
    val bit8vectors = bits.chunkN(8).map(c => BitVector.bits(c.toArray))
    bit8vectors.filter(_.sizeGreaterThanOrEqual(8)).map(_.toByte())
  }

  def decode1[F[_]: RaiseThrowable]: Pipe[F, Char, BitVector] = _.flatMap { char =>
    val cInt = char.toInt
    if (Blocks.lookupDecodeZero.contains(cInt)) {
      val byte = Blocks.lookupDecodeZero(cInt)
      val bits = BitVector.fromShort(byte.toShort, 15)
      Stream(bits)
    } else if (Blocks.lookupDecodeOne.contains(cInt)) {
      val byte = Blocks.lookupDecodeOne(cInt)
      val bits = BitVector.fromByte(byte.toByte, 7)
      Stream(bits)
    } else {
      Stream.raiseError[F](new IllegalArgumentException(s"Invalid character: $char"))
    }
  }

}
