package tf.bug.base32768

object Constants {

  val blockSize: Int = 32
  val encodingSize: Int = 15
  val byteSize: Int = 8

}
