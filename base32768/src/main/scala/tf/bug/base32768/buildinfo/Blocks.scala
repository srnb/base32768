package tf.bug.base32768.buildinfo

import scodec.bits.ByteVector
import tf.bug.base32768.Constants

object Blocks {

  lazy val zero: Vector[Char] = BuildInfo.zeroblocks.toCharArray.toVector
  lazy val one: Vector[Char] = BuildInfo.oneblocks.toCharArray.toVector

  def lookupEncode(v: Vector[Char]): Map[Int, Int] =
    v.zipWithIndex.flatMap {
      case (chr, i) =>
        val startCodePoint = chr.toInt
        val startBlockPosition = Constants.blockSize * i
        (0 until Constants.blockSize).map { offset =>
          val codePoint = startCodePoint + offset
          val blockPosition = startBlockPosition + offset
          blockPosition -> codePoint
        }
    }.toMap

  lazy val lookupEncodeZero: Map[Int, Int] = lookupEncode(zero)

  lazy val lookupEncodeOne: Map[Int, Int] = lookupEncode(one)

  def lookupDecode(v: Vector[Char]): Map[Int, Int] =
    v.zipWithIndex.flatMap {
      case (chr, i) =>
        val startCodePoint = chr.toInt
        val startBlockPosition = Constants.blockSize * i
        (0 until Constants.blockSize).map { offset =>
          val codePoint = startCodePoint + offset
          val blockPosition = startBlockPosition + offset
          codePoint -> blockPosition
        }
    }.toMap

  lazy val lookupDecodeZero: Map[Int, Int] = lookupDecode(zero)

  lazy val lookupDecodeOne: Map[Int, Int] = lookupDecode(one)

}
