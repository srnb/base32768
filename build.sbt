import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}

import scala.io.Source

val sharedSettings = Seq(
  organization := "tf.bug",
  name         := "base32768",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  libraryDependencies ++= Seq(
    "org.scodec" %%% "scodec-core"    % "1.11.3",
    "org.scodec" %%% "scodec-bits"    % "1.1.10",
    "co.fs2" %%% "fs2-core" % "1.0.4",
  ),
  buildInfoPackage := "tf.bug.base32768.buildinfo",
  buildInfoKeys := Seq(
    BuildInfoKey.action("zeroblocks") {
      val zkf = file("zeroblocks.txt")
      val src = Source.fromFile(zkf)("UTF-8")
      val s = src.getLines().mkString
      src.close()
      s
    },
    BuildInfoKey.action("oneblocks") {
      val okf = file("oneblocks.txt")
      val src = Source.fromFile(okf)("UTF-8")
      val s = src.getLines().mkString
      src.close()
      s
    },
  ),
)

lazy val base32768 =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .enablePlugins(BuildInfoPlugin)
    .settings(sharedSettings)
    .jsSettings(scalaJSUseMainModuleInitializer := true)
    .jvmSettings( /* ... */ )

lazy val base32768JS = base32768.js
lazy val base32768JVM = base32768.jvm
