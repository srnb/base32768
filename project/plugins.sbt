addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "0.6.0")
addSbtPlugin("org.scala-js"       % "sbt-scalajs"              % "0.6.27")
addSbtPlugin("com.eed3si9n"       % "sbt-buildinfo"            % "0.7.0")
